<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Crayon</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content='user-scalable=no, width=device-width' name='viewport'>
        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="style.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <!--[if lt IE 7]><link rel="stylesheet" href="css/ie.css"><![endif]-->
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        
        <div class="leftStatic stockistsPage">
	        <header>
				<a href="index.php">
	        		<div class="logo">
		        			<!--[if IE]><img src="i/logo.png" alt="logo"/><![endif]-->
									<img class="svg" src="i/logo.svg" alt="logo"/>
	        		</div>
	        	</a>
		       <nav class="main clearfix">
				        <ul>
				         	<li><a href="mens.php">Mens.</a></li>
					        <li><a href="womans.php">Womans.</a></li>
					        <li><a href="about.php">About Us.</a></li>
					        <li><span class="grey">Stockists.</span></li>
					        <li><a href="contact.php">Contact.</a></li>
				        </ul>
			        </nav>
	        </header>
	        
        </div>
        <div class="right">
	        <div class="stockists">
		       <h3>Stockists.</h3>
		       <div class="scrollAble">
						<p>	<span class="storeHeader">187 Street Wear. </span>
								<span class="storeAdd">Shop 10. Downtown Complex.<br /> 38 - 60 Broadway ave. Palmerston North.<br /> Phone 06 358 8831</span>
						</p>
						<p>	<span class="storeHeader">A Little Bit Different.</span>
								<span class="storeAdd">Maniapoto Street. Otorohanga. <br /> Phone 07 873 7367 </span>
						</p>
						<p>	<span class="storeHeader">Arizona.</span> 
								<span class="storeAdd">West City Mall. Catherine St. Henderson. Auckland.<br />Phone 09 836 5980</span>
						</p>
						<p>	<span class="storeHeader">Base.</span>
								<span class="storeAdd">Cnr Helwick St And Dunmore St. Wanaka 9305. <br />Phone 03 443 6699</span>
						</p>
						<p>	<span class="storeHeader">Bms.</span> 
								<span class="storeAdd">22 Gladstone Rd. Gisbourne. <br />Phone 06 867 8424</span>
						</p>
						<p>	<span class="storeHeader">District Clothing Ltd.</span> 
								<span class="storeAdd">146 King St. Pukekohe 2120. <br />Phone 09 238 5336</span>
						</p>
						<p>	<span class="storeHeader">Evolve Surf & Skate.</span> 
								<span class="storeAdd">Suite 11B - 34 Gravatt Rd. Papamoa Beach. Papamoa. <br />Phone 07 575 5728	</span>
						</p>	
						<p>	<span class="storeHeader">Id.</span> 
								<span class="storeAdd">Ck Rarotonga. 11 Manu Tapu Dr. Auckland Airport. Auckland 2022 <br />Phone +682 26238</span>
						</p>
	       
						<p>	<span class="storeHeader">Thomas'S.</span> 
								<span class="storeAdd">54 Market St. Blenheim. <br />Phone 021 321 878</span>
						</p>
						<p>	<span class="storeHeader"> Tokoroa Clothing Company.</span> 
								<span class="storeAdd">Leith Place. Tokoroa. <br />Phone 07 886 8488</span>
						</p>
						
						<p>	<span class="storeHeader">Saltwater Surf Co Ltd.</span>
								<span class="storeAdd">505 Port Rd. Whangamata.<br /> Phone 07 865 8666</span>
						</p>
						
						<p>	<span class="storeHeader">Urban Beach.</span>
								<span class="storeAdd">183 Trafalgar St . Nelson</span>
						</p>
						
						<p>	<span class="storeHeader">Urban Edge. </span>
								<span class="storeAdd">Cnr Gascoigne & Tamamutu Streets. Taupo. <br />Phone 07 378 7400</span>
						</p>
						<p>	<span class="storeHeader">Urban Surf. </span>
								<span class="storeAdd">25D Marriner St. Sumner. Christchurch. <br />Phone 03 326 6023</span>
						</p>							
						<p>	<span class="storeHeader">Void. </span>
								<span class="storeAdd">8 Albion Place. Dunedin. <br />Phone 03 474 1110</span>
						</p>
		       </div>
					</div>
        </div>

		

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>