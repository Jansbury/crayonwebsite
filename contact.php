<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Crayon</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content='user-scalable=no, width=device-width' name='viewport'>
        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="style.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <!--[if lt IE 7]><link rel="stylesheet" href="css/ie.css"><![endif]-->
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        
        <div class="leftStatic contactPage">
	        <header>
				<a href="index.php">
	        		<div class="logo">
		        			<!--[if IE]><img src="i/logo.png" alt="logo"/><![endif]-->
									<img class="svg" src="i/logo.svg" alt="logo"/>
	        		</div>
	        	</a>
		        <nav class="main clearfix">
			        <ul>
			         	<li><a href="mens.php">Mens.</a></li>
				        <li><a href="womans.php">Womans.</a></li>
				        <li><a href="about.php">About Us.</a></li>
				        <li><a href="stockists.php">Stockists.</a></li>
				        <li><span class="grey">Contact.</span></li>
			        </ul>
		        </nav>
	        </header>
	        
        </div>
        <div class="right">
	        <div class="aboutUs">
		       <h3>Contact Us.</h3>
						<p>All enquiries please contact Brent Cooper.</p>
						<p><a class="email" href="mailto:brent@hotice.co.nz">brent@hotice.co.nz</a></p>
						<p>Ph: 021 599915</p>
						<p>Fax: 09 8378243</p>
	        </div>
        </div>

		

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>