<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
         <meta charset="utf-8">
        <title>Crayon</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content='user-scalable=no, width=device-width' name='viewport'>
        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="style.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <!--[if lt IE 7]><link rel="stylesheet" href="css/ie.css"><![endif]-->
    </head>
    <body id="swipe">
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        <div class="left">
        <!--[if IE]><style>#preloader{display:none;}</style><![endif]--> 
        <div id="preloader"><div id="loader">&nbsp;</div></div>
	        <div class="imgSlider">
		        <header>
		        	<a href="index.php">
		        		<div class="logo">
			        		<!--[if IE]><img src="i/logo.png" alt="logo"/><![endif]-->
									<img class="svg" src="i/logo.svg" alt="logo"/>
		        		</div>
		        	</a>

			        <nav class="main clearfix">
				        <ul>
				         	<li><span class="grey">Mens.</span></li>
					        <li><a href="womans.php">Womans.</a></li>
					        <li><a href="about.php">About Us.</a></li>
					        <li><a href="stockists.php">Stockists.</a></li>
					        <li><a href="contact.php">Contact.</a></li>
				        </ul>
			        </nav>
		        	<div class="navToggle clearfix"></div>
		        </header>
	        </div>
        </div>
        
        <div class="right">
	        <div class="slider">
            <section>
						 <div class="products">
               <div class="description">
				       	<h4> Excess Crew Neck Tee.</h4>
				       	<p class="italicStyle">N&ordm;. 9001</p>
				       	<p class="colors">Available in Arctic Blue, Asphalt Marle, Black, Burgundy,
									Forest Marle. Grey Marle, Navy, Red & White.
								</p>
								<p class="italicStyle">Sizes Sml through 2XL.</p>
				       </div>
						 </div>
            </section><!--end #one-->
               
            <section>
						 <div class="products">
               <div class="description">
				       	<h4>Standard Crew Neck Tee.</h4>
				       	<p class="italicStyle">N&ordm;. 9002</p>
				       	<p class="colors">Available in Arctic Blue,Black,Charcoal, Dark Chocolate, . Grey Marle, Navy, Red, Royal Blue, Teal & White.</p>
								<p class="italicStyle">Sizes XS through 2XL.</p>
				       </div>
						 </div>
            </section><!--end #one-->

            <section>
						 <div class="products">
               <div class="description">
				       	<h4>V-Neck Tee</h4>
				       	<p class="italicStyle">N&ordm;. 9003</p>
				       	<p class="colors">Available in Black & White.</p>
								<p class="italicStyle">Sizes Sml through 2XL.</p>
				       </div>
						 </div>
            </section><!--end #one-->
            
            
            <div class="hiddenImg">
	            <img src="i/9205.Model.Image.jpg" alt="fourth caption" />
	            <img src="i/8019.Model.Image.jpg" alt="fourth caption" />
	            <img src="i/8014.Model.Image.jpg" alt="fourth caption" />
            </div>
            
               
            	
	        </div>
        </div>
				<div class="nextBttn plus"></div>
        <div class="prevBttn minus"></div>
		

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script src="js/vendor/jquery.easing-1.3.min.js"></script>
        
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>