
$("#swipe").swipe({
  swipeRight:function(event, direction, distance, duration, fingerCount) {
	if (currentIndex > 0) {
		newIndex = currentIndex - 1;
	} else {
		newIndex = lastIndex;
	}
	slideToImage(newIndex);
	$(".left").backstretch("prev").backstretch("pause");
	},
	swipeLeft:function(event, direction, distance, duration, fingerCount) {
	if (currentIndex < lastIndex) {
			newIndex = currentIndex + 1;
		} else {
			newIndex = 0;
		}
		slideToImage(newIndex);
		$(".left").backstretch("next").backstretch("pause");
  }
});

//backstretch applications
var imgArray = $(".hiddenImg img").map(function() {
  return $(this).attr("src");
}).get();

if ($(".left").length > 0) {
	$(".left").backstretch(imgArray, { fade: 350}).backstretch("pause");
}
//nav and logo
$('.logo img').hover( 
  function() {
    $(this).css({"opacity": .7})
  },
  function() {
    $(this).css({"opacity": 1})
});

$(".navToggle").toggle(function(){
	  $("nav.main").stop().animate({ left: "-=100" }, 1000, "easeOutExpo")
	  $("nav.main").stop().animate({ left: "-=100" }, 1000, "easeOutExpo")
	  $(".logo img").css({"opacity": 0.1})
	  $('.minus').addClass("scaleUp")
     .delay(200)
     .queue(function() {
     		$(this).removeClass("scaleUp");
        $(this).addClass("scaleDown");
        $(this).dequeue().removeClass("scaleDown");
     });
		 $('.plus').addClass("scaleUp")
     .delay(200)
     .queue(function() {
     		$(this).removeClass("scaleUp");
        $(this).addClass("scaleDown");
        $(this).dequeue().removeClass("scaleDown");
     });
}, function(){
		$("nav.main").stop().animate({ left: "+=100" }, 1000, "easeOutExpo")
		$("nav.main").stop().animate({ left: "+=100" }, 1000, "easeOutExpo")
		$(".logo img").css({"opacity": 1})
});    

$('.navToggle').click(function(e) {
	var $arrowVar = $('.navToggle');
		e.preventDefault();
	if ($arrowVar.hasClass("rotate180")) {
		$arrowVar.removeClass("rotate180").addClass("rotate0");
	} else {
		$arrowVar.addClass("rotate180").removeClass('rotate0');
	};
});

if ($(".contactPage").length > 0) {
	$(".contactPage").backstretch("i/static/contact.jpg", { fade: 350});
}
if ($(".leftStatic").length > 0) {
	$(".aboutPage").backstretch("i/static/about.jpg", { fade: 350});
}
if ($(".stockistsPage").length > 0) {
	$(".stockistsPage").backstretch("i/static/stockists.jpg", { fade: 350});
}
if ($(".homeRight").length > 0) {
	$(".homeRight").backstretch("i/static/home.jpg", { fade: 350});
}

// pollen slider	
var $nextBtn = $('.nextBttn'),
	$prevBtn = $('.prevBttn'),
	slideSpeed = 1000,
	$right = $('.right'),
	$panels = $('.slider section'),
	rightWidth = $right.width(),
	$slider = $('.slider'),
	numPanels = $panels.size(),
	lastIndex = numPanels - 1,
	$sliderWidth = rightWidth * numPanels,
	currentIndex = 0;
	resizeMe();
$(window).resize(function() {
	resizeMe();
});

function resizeMe() {
	$wH = $(window).height();
	rightWidth = $right.width();
	$sliderWidth = rightWidth * numPanels;
	$slider.width($sliderWidth);
	$panels.width(rightWidth);
	$panels.height($wH);
	var slideDist = currentIndex * -rightWidth;
	$slider.css({
		"margin-left": slideDist
	});
}
$nextBtn.click(function() {
	if (currentIndex < lastIndex) {
		newIndex = currentIndex + 1;
	} else {
		newIndex = 0;
	}
	slideToImage(newIndex);
	$(".left").backstretch("next").backstretch("pause");
});
$prevBtn.click(function() {
	if (currentIndex > 0) {
		newIndex = currentIndex - 1;
	} else {
		newIndex = lastIndex;
	}
	slideToImage(newIndex);
	$(".left").backstretch("prev").backstretch("pause");
});

function slideToImage(newIndex) {
	currentIndex = newIndex;
	var slideDist = currentIndex * -rightWidth;
	$slider.animate({
		"margin-left": slideDist
	}, slideSpeed, "easeOutExpo");
}
//preoader
$(window).load(function() { 
    $("#status").fadeOut(); 
    $("#preloader").delay(100).fadeOut("slow"); 
})
